load '/home/kenneth/5386f807-kenneth.cheston-tic-tac-toe/lib/game.rb'

class ComputerPlayer

  attr_reader :name, :board
  attr_accessor :mark


  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
    #do not like this render method, made a new method in board class
    #this is simply active to pass specs
    print board.grid
  end

  def get_move
    kill_move = @board.check_kill_move(@mark)
    return kill_move unless kill_move == nil

    @board.shuffle_move
  end




end
  #
  # what is the rationale here??
  #
  # FIRST: we want to see if a kill move is available...
  #
  # THEN: we want to find the missing move with the help of the instance variable "index"
  #



#
# board = Board.new
# comp = ComputerPlayer.new
#
#
# board.place_marks([[0, 0], [1, 0]], :O)
# comp.mark = :O
# # comp.display(board)
# p comp.show_indices(board)




# 0  0:O 1:nil 2:nil
# 1  0:O 1:nil 2:nil
# 2  0:nil 1:nil 2:nil


# [[:O, nil, nil], [:O, nil, nil], [nil, nil, nil]]


# [0,0], [1,0]
