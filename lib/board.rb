class Board

  attr_reader :grid

  def initialize(grid_input = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid_input
    @right_diagonal = [[0, 2], [1, 1], [2, 0]]
    @left_diagonal = [[0, 0], [1, 1], [2, 2]]
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]].nil?
  end

  def winner
    return :X if check_for_symbol(:X)
    return :O if check_for_symbol(:O)
    return nil
  end

  def check_for_symbol(sym)

    #rows
    @grid.each_with_index do |row, index|
      return true if row.all? {|pos| pos == sym}
    end

    #columns
    @grid.each_with_index do |row, index_01|
      @grid.each_with_index do |el, index_02|
        column = @grid[index_02]
        break if column[index_01] != sym
        return true if index_02 == 2
      end
    end

    #diagonals

    return true if @right_diagonal.all? do |pos|
      row = @grid[(pos[0])]
      column = (pos[1])
      row[column] == sym
    end
    return true if @left_diagonal.all? do |pos|
      row = @grid[(pos[0])]
      column = (pos[1])
      row[column] == sym
    end
    false
  end

  def nil?(num)
    return num if num != nil
    return " "
  end

  def render


    a = "#{nil?(@grid[0][0])}"
    b = "#{nil?(@grid[0][1])}"
    c = "#{nil?(@grid[0][2])}"
    d = "#{nil?(@grid[1][0])}"
    e = "#{nil?(@grid[1][1])}"
    f = "#{nil?(@grid[1][2])}"
    g = "#{nil?(@grid[2][0])}"
    h = "#{nil?(@grid[2][1])}"
    i = "#{nil?(@grid[2][2])}"
    puts "

    #{a} | #{b} | #{c}
    ---------
    #{d} | #{e} | #{f}
    ---------
    #{g} | #{h} | #{i}

      "
  end

  def check_kill_move(sym)

    #row
    @grid.each_with_index do |row, index|
      if row.count { |pos| pos == sym } == 2
        idx = row.index { |pos| pos == nil }
        return [index, idx] if idx != nil
      end
    end

    #column
    @grid.each_with_index do |el, idx|
      column_index = []
     @grid.each_with_index do |el_2, idx_2|
        column_index << @grid[idx_2][idx]
        if idx_2 == 2
          tally = column_index.count { |pos| pos == sym }
          if tally == 2 && column_index.include?(nil)
            row = column_index.index(nil)
            return [row, idx]
          end
          column_index = []
        end
      end
    end

    #diagonals
    @right_diagonal.each_with_index do |pos, idx|
      row = @grid[(pos[0])]
      column = (pos[1])
      marks = []
      marks << row[column]
      if marks.count { |mark| mark == sym } == 2 && marks.include?(nil)
        return @right_diagonal[marks.index(nil)]
      end
    end

    @left_diagonal.each_with_index do |pos, idx|
      row = @grid[(pos[0])]
      column = (pos[1])
      marks = []
      marks << row[column]
      if marks.count { |mark| mark == sym } == 2 && marks.include?(nil)
        return @left_diagonal[marks.index(nil)]
      end
    end
    nil
  end

  def shuffle_move
    sub_array = [0, 1, 2]
    while sub_array
      row = sub_array.shuffle[0]
      column = sub_array.shuffle[0]
      if @grid[row][column] == nil
        return [row,column]
      end
    end
  end

  def over?
    return true if winner == nil && !(@grid.any? { |pos| pos.include?(nil) }) ||
    winner != nil
  end

  def place_marks(marks, sym)
    marks.each { |pos| place_mark(pos, sym) }
  end
end
#
# board = Board.new
#
# board.place_marks([[0, 0], [0, 2], [2, 0], [2, 1]], :X)
# board.place_marks([[1, 0], [0, 1]], :O)
#
# p board.check_kill_move(:X)
#





# 0  0:X 1:nil 2:nil
# 1  0:X 1:nil 2:nil
# 2  0:nil 1:nil 2:nil


# [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]]

#indices = [0,0], [0,1] && [0,2] == nil

#grid.each_with_index do |el, idx|
  #sub_array = []
 #grid.each_with_index do |el_2, idx_2|
    #sub_array << @grid[idx_2][idx]
    #if idx_2 == 2
      #tally = sub_array.count { |pos| pos == sym }
      #if tally == 2 && sub_array.include?(nil)
        # row = sub_array.index(nil)
        # return [row, idx]
      #end
    #else tally = []
  #end
#end


#@grid[0][0]
#@grid[1][0]
#@grid[2][0]

#@grid[1][0]
#@grid[1][1]
#@grid[1][2]
