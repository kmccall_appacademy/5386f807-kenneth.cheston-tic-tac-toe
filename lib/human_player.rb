load '/home/kenneth/5386f807-kenneth.cheston-tic-tac-toe/lib/board.rb'

class HumanPlayer


  attr_reader :name, :board


  #
  def initialize(name)
    @name = name
  end


  def display(board)
    @board = board
    #do not like this render method, made a new method in board class
    #this is simply active to pass specs
    print board.grid
  end

  def get_move
    puts "Where would you like to put your mark?"
    move = gets.chomp
    move.split(",").map(&:to_i)

    #code below was used to reject non-valid moves


    # index = 0
    # while index < 10
    #
    #   move = gets.chomp
    #   position = move.split(",").map(&:to_i)
    #   if (@board.grid)[position[0]][position[1]] != nil
    #     puts "Not a valid move, try again!"
    #   else return move.split(",").map(&:to_i)
    #   end
    #   index += 1
    # end
  end

end

# board = Board.new
# human = HumanPlayer.new
#
#
# board.place_mark([0, 0], :X)
#
# p human.display(board)
