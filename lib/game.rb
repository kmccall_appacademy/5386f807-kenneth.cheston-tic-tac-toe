# load '/home/kenneth/5386f807-kenneth.cheston-tic-tac-toe/lib/board.rb'

require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'


class Game

  attr_reader :board, :player_one, :player_two, :current_player


  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new()
    @current_player = player_one
  end

  def play
    puts "Welcome to Tic-Tac-Toe #{@current_player.name}!"
    while !(@board.over?)
      @board.render
      @current_player.display(@board)
      play_turn
      if @board.winner != nil
        return puts "You Won!" if @board.winner == :X
        return puts "You Lost :(" if @board.winner == :O
      switch_players!
      end
    end
    puts "It's a draw!"
  end

  def play_turn
    mark = nil
    move = @current_player.get_move
    @current_player == player_one ? mark = :X : mark = :O
    board.place_mark(move, mark)
    switch_players!
  end

  def switch_players!
    @current_player == player_one ? @current_player = player_two : @current_player = player_one
  end


  if __FILE__ == $PROGRAM_NAME
    player_one = HumanPlayer.new("Ken")
    player_two = ComputerPlayer.new("Douchebag")
    game = Game.new(player_one, player_two)
    game.play
  end


end
